<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Account>
 */
class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
     
        $users_count=DB::table('users')->count();
        return [
            'balance' => rand(0,1000),
            'currency' => $this->faker->currency(),
            'user_id' => random_int(1,$users_count)
        ];
    
    }
}
