<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts_count=DB::table('accounts')->count();
        DB::table('Transactions')->insert([
            'from' => random_int(1,$accounts_count),
            'to' => random_int(1,$accounts_count),
            'amount' => rand(0,1000),
            'currency' => $this->faker->currency(),
        ]);
    }
}
