<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users_count=DB::table('users')->count();
        DB::table('Accounts')->insert([
            'balance' => rand(0,1000),
            'currency' => $this->faker->currency(),
            'user_id' => random_int(1,$users_count)
        ]);

    }
}
