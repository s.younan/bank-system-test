<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\userController;
use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/accounts', [AccountController::class, 'createAccount']);

Route::put('/accounts', [AccountController::class, 'updateAccount']);

Route::get('/accounts',[AccountController::class, 'getAccount']);

Route::get('/user/accounts',[AccountController::class, 'getAccounts']);

Route::delete('/accounts',[AccountController::class, 'deleteAccount']);
