<?php

use App\Http\Livewire\CreateTransaction;
use App\Http\Livewire\Login;
use App\Http\Livewire\ShowTransactions;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ShowTransactions::class)->middleware('auth');

Route::get('/login', Login::class)->middleware('guest')->name('login');

Route::get('/create-transaction', CreateTransaction::class)->middleware('auth')->name('create-transaction');
