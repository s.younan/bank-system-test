<div>
  <table class="table">
      <thead>
        <tr>
          @foreach ($titles as $title)
            <th class="header">{{ $title }}</th>
          @endforeach
        </tr>
      </thead>
      <tbody>
        @foreach ($records as $record)
          <tr>
              @foreach ($titles as $title)
              <td class="data">{{ $record[$title] }}</td>
              @endforeach
          </tr>
        @endforeach
      </tbody>
  </table>
</div>