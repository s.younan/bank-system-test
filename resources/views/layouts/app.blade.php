<link rel="stylesheet" href="{{ mix('css/app.css') }}">
<head>
    @livewireStyles
</head>
<body>
    @if (Auth::check())
        <livewire:logout />  
    @endif
    {{ $slot }}

    @livewireScripts
</body>