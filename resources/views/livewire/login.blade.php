<div class="form-div">
  
  <div class="form">

    <div class="imgcontainer">
      <img src={{ asset('logo.png') }} alt="Logo" class="logo">
    </div>

    <div class="form-container">
      <label for="email">Email</label>
      <input wire:model="email" type="email" name="email" required>
      @error('email') <span class="error">{{ $message }}</span> @enderror

      <label for="password">Password</label>
      <input wire:model="password" type="password" name="password" required>
      @error('password') <span class="error">{{ $message }}</span> @enderror
        
      <label>
        <input wire:model="remember" type="checkbox" name="remember"> Remember me
      </label>
      @error('remember') <span class="error">{{ $message }}</span> @enderror

      <button class="login" wire:click="login"><b>LOGIN</b></button>

    </div>

  </div>

  <div>
    @if (session()->has('message'))
        <div class="alert alert-success" class="alert">
            <B>{{ session('message') }}</B>
        </div>
    @endif
  </div>
</div>