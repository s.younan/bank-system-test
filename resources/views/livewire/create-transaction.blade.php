<div class="form-div">

    <div class="createTransactionForm">
      <h1 class="createTransactionTitle">Create Transaction</h1>
      <div class="form-container">

        <label for="from">Transaction From</label>
        <livewire:search-drop-down :options="$user_accounts" :select_event="'setFrom'"/>
        @error('from') <span class="error">{{ $message }}</span> @enderror
  
        <label for="to">Transaction To</label>
        <input wire:model="to" type="number" name="to" required>
        @error('to') <span class="error">{{ $message }}</span> @enderror
          
        <label for="amount">Amount</label>
        <input wire:model="amount" type="number" name="amount" required>
        @error('amount') <span class="error">{{ $message }}</span> @enderror

        <label for="currency">Currency</label>
        <livewire:search-drop-down :options="$currencies" :select_event="'setCurrency'"/>
        @error('currency') <span class="error">{{ $message }}</span> @enderror
  
        <button class="create" wire:click="createTransaction"><b>Create</b></button>
  
      </div>
  
    </div>
  
    <div>
      @if (session()->has('message'))
          <div class="created">
              <B>{{ session('message') }}</B>
          </div>
      @endif
    </div>
  </div>
