<div class="container">
    <button class="createTransaction" wire:click="createTransaction"><b>Create Transaction</b></button>
    <h1 class="title">User Transactions</h1>
    <x-table :titles="$titles" :records="$user_transactions"/>
    <div class="pagination"> {{ $user_transactions->links() }} </div>
</div>