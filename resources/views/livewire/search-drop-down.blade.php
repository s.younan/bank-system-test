<div>
        <div>
                <input wire:click="showOptions()" wire:model.debounce.500ms="option" class="search-input" placeholder="Search..."/>
                @if(strlen($this->option)>0)
                    @if($show_options)
                        @if(count($search_options)>0)
                                <ul class="dropdown">
                                        <div class="dropdown-content">
                                        @foreach($search_options as $search_option)
                                                <li wire:click="selectOption('{{$search_option}}')">{{$search_option}}</li>
                                        @endforeach
                                        </div>
                                </ul>
                        @else
                                <ul class="dropdown-e">
                                        <div class="dropdown-empty">
                                                <li>Not Found...</li>
                                        </div>
                                </ul>  
                        @endif
                    @endif  
                @endif
        </div>
</div>
