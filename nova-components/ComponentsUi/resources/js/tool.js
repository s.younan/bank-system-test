import ButtonField from './components/ButtonField/ButtonField.vue'
import DropDownField from './components/DropDownField/DropDownField.vue'
import NumericField from './components/NumericField/NumericField.vue'

Nova.booting((Vue, router, store) => {
  Vue.component('button-field', ButtonField)
  Vue.component('drop-down-field', DropDownField)
  Vue.component('numeric-field', NumericField)
})
