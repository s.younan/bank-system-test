function parseResponse(response, titles) {
  var data = {
    records:[],
    pages:1
  }

    data.pages= response.data.total/response.data.per_page;
    data.records=[];
    (response.data.resources).forEach(resource => {
      var record={};
      (resource.fields).forEach(field => {
        if(titles.includes(field.name))
          record[field.name]=field.value;
      });
      data.records.push(record);
    });

  return data;
}

export default parseResponse;