import {createAccountModule, currencyModule} from "./store/index"
Nova.booting((Vue, router, store) => {
  router.addRoutes([
    {
      name: 'create-account',
      path: '/create-account',
      component: require('./components/Tool'),
    },
  ])
  store.registerModule('createAccountModule', createAccountModule);
  store.registerModule('currencyModule', currencyModule);
})