import createAccountModuleindex from './CreateAccountModule/index'
import currencyModuleindex from './CurrencyModule/index'

const createAccountModule= createAccountModuleindex;
const currencyModule= currencyModuleindex;

export {createAccountModule, currencyModule};