import {CURRENCIES_VALUES_SETTING_MUTATION} from './mutations'

const actions = {
    setCurrencies ({ commit }) {
      axios.get("https://openexchangerates.org/api/currencies.json").then(response => {
          var currencies= Object.keys(response.data);
          commit(CURRENCIES_VALUES_SETTING_MUTATION , currencies);
      })
      .catch(function (error) {
          Nova.error('Currencies Loading Failed');
      })
    }
  }

const CURRENCIES_VALUES_SETTING_ACTION= 'setCurrencies'

export {actions, CURRENCIES_VALUES_SETTING_ACTION};