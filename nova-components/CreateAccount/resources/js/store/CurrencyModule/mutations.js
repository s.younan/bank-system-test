const mutations = {
  setCurrencies(state, currencies){
    state.currencies=currencies;
  }
}

const CURRENCIES_VALUES_SETTING_MUTATION= 'setCurrencies'

export {mutations, CURRENCIES_VALUES_SETTING_MUTATION};