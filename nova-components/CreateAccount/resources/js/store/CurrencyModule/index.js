import states from './states'
import {mutations} from './mutations'
import {actions} from './actions'

const currencyModule = {
    state: states,
    mutations: mutations,
    actions: actions
  }
  
export default currencyModule;