const getters = {
    getBalance (state) {
        return state.balance
    },
    getCurrency (state) {
        return state.currency
    },
}

const BALANCE_GETTER= 'getBalance'
const CURRENCY_GETTER= 'getCurrency'

export {getters, BALANCE_GETTER, CURRENCY_GETTER}