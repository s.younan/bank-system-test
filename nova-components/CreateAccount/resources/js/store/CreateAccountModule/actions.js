import {CREATE_ACCOUNT_RESET_STATE_MUTATION} from './mutations'
import {CREATE_ACCOUNT_SET_BALANCE_MUTATION, CREATE_ACCOUNT_SET_CURRENCY_MUTATION} from './mutations'

const actions = {
  setBalance({commit}, balance){
    commit(CREATE_ACCOUNT_SET_BALANCE_MUTATION, balance);
  }, 
  setCurrency({commit}, currency){
    commit(CREATE_ACCOUNT_SET_CURRENCY_MUTATION, currency);
  },  
  submit ({ state, commit }) {
      var data={
        user: Nova.config.userId,
        balance: state.balance,
        currency: state.currency
      };
      Nova.request().post("/nova-api/accounts?editing=true&editMode=create", data).then(response => {
        commit(CREATE_ACCOUNT_RESET_STATE_MUTATION);
        Nova.success("Account Created");
      })
      .catch(function (error) {
        Nova.error("Account Creation Failed");
      });
    }
  }

const CREATE_ACCOUNT_SUBMIT_ACTION= 'submit'
const SET_BALANCE_ACTION= 'setBalance'
const SET_CURRENCY_ACTION= 'setCurrency'

export {actions, CREATE_ACCOUNT_SUBMIT_ACTION, SET_BALANCE_ACTION, SET_CURRENCY_ACTION};