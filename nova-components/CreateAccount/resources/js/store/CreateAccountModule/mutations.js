import {INITIAL_STATE_BALANCE, INITIAL_STATE_CURRENCY} from './states'

const mutations = {
    setBalance (state, balance) {
      state.balance = balance;
    },
    setCurrency (state, currency) {
      state.currency = currency;
    },
    resetState (state) {
      state.balance=INITIAL_STATE_BALANCE;
      state.currency=INITIAL_STATE_CURRENCY;
    }
  }

const CREATE_ACCOUNT_SET_BALANCE_MUTATION= 'setBalance'
const CREATE_ACCOUNT_SET_CURRENCY_MUTATION= 'setCurrency'
const CREATE_ACCOUNT_RESET_STATE_MUTATION= 'resetState'

export {mutations, CREATE_ACCOUNT_SET_BALANCE_MUTATION, CREATE_ACCOUNT_SET_CURRENCY_MUTATION, CREATE_ACCOUNT_RESET_STATE_MUTATION};