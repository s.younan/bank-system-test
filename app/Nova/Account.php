<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Silber\Bouncer\BouncerFacade as Bouncer;

class Account extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Account::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    public static $with = ['user','fromTransactions','toTransactions'];


    public static $perPageOptions = [5, 25, 50, 100];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            BelongsTo::make('User'),

            Number::make('Balance')
            ->sortable()
            ->rules('required', 'min:0'),

            Text::make('Currency')
            ->sortable()
            ->rules('required', 'max:50'),

            HasMany::make('From Transactions','fromTransactions','App\Nova\Transaction'),

            HasMany::make('To Transactions ','toTransactions', 'App\Nova\Transaction'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\UserName,
            new Filters\UserEmail,
            new Filters\CreatedBefore,
            new Filters\CreatedAfter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function availableForNavigation(Request $request)
    {
        return Bouncer::is($request->user())->an('admin');
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        if(Bouncer::is($request->user())->an('admin'))
            return $query;
        return $query->where('user_id', $request->user()->id);
    }
}