<?php

namespace App\Http\Livewire;

use App\Models\Account;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class ShowTransactions extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $titles = ['id', 'from', 'to', 'amount', 'currency'];
    protected $user_transactions;

    public function render()
    {
        $this->fetchTransactions();
        return view('livewire.show-transactions', ['user_transactions'=>$this->user_transactions, 'titles'=>$this->titles]);
    }

    public function fetchTransactions()
    {
        $user=Auth::id();

        $user_accounts= Account::where('user_id',$user)
                                ->pluck('id')
                                ->toArray();

        $this->user_transactions= Transaction::whereIn('from',$user_accounts)
                                            ->orWhereIn('to',$user_accounts)
                                            ->paginate(5);
    }

    public function createTransaction()
    {
        return redirect('/create-transaction');
    }
}