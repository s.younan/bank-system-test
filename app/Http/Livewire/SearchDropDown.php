<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SearchDropDown extends Component
{
    public $options;
    public $search_options= [];
    public $option= '';
    public $show_options=false;
    public $select_event;

    protected $listeners = [ 'resetVars' ];
    
    public function selectOption($search_option)
    {
        $this->option=$search_option;
        $this->show_options=false;
        $this->emit($this->select_event, $this->option);
    }

    public function showOptions()
    {
        $this->show_options=true;
    }

    public function resetVars()
    {
        $this->option='';
    }

    public function render()
    {
        if(strlen($this->option)>0){
            $this->search_options= [];
            foreach ($this->options as $option) {
                if(str_contains(strtolower(strval($option)), strtolower(strval($this->option))))
                    array_push($this->search_options,$option);
            }
        }
        return view('livewire.search-drop-down');
    }
}
