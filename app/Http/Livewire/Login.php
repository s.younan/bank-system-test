<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{   
    public $email;
    public $password;
    public $remember= false;

    protected $rules = [
        'email' => 'required|String|email',
        'password' => 'required|String',
        'remember' => 'Boolean'
    ];

    public function render()
    {
        return view('livewire.login');
    }

    public function login()
    {
        $this->validate();

        if (Auth::attempt(['email' => $this->email, 'password' => $this->password], $this->remember)){
            return redirect()->to('/');
        }
        
        session()->flash('message', 'Incorrect Email/Password');
    }
}
