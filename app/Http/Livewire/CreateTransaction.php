<?php

namespace App\Http\Livewire;

use App\Models\Account;
use App\Models\Transaction;
use GuzzleHttp\Client;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CreateTransaction extends Component
{

    public $user_accounts=[];
    public $currencies=[];
    public $from='';
    public $to='';
    public $amount='';
    public $currency='';

    protected $rules = [
        'from' => 'required|exists:accounts,id',
        'to' => 'required|exists:accounts,id',
        'amount' => 'required|numeric|gt:0',
        'currency' => 'required|string'
    ];

    protected $listeners = [ 'setFrom', 'setCurrency', 'resetVars' ];

    public function render()
    {
        return view('livewire.create-transaction');
    }

    public function mount()
    {
        $this->fetchUserAccounts();
        $this->fetchCurrencies(); 
    }

    public function setFrom($from)
    {
        $this->from= $from;
    }

    public function setCurrency($currency)
    {
        $this->currency= $currency;
    }

    public function createTransaction()
    {
        $this->validate();

        $from= Account::findOrFail($this->from);
        $to= Account::findOrFail($this->to);

        $remove_from= $this->convertCurrency($this->amount,$this->currency,$from->currency);
        $add_to= $this->convertCurrency($this->amount,$this->currency,$to->currency);

        if($from->balance>=$remove_from){
            $from_balance= $from->balance - $remove_from;
            $to_balance= $to->balance + $add_to;

            $from->balance= $from_balance;
            $to->balance= $to_balance;

            $from->save();
            $to->save();

            try{
                if($this->from!=$this->to){
                    Transaction::insert([
                        'from' => $this->from,
                        'to' => $this->to,
                        'amount' => $this->amount,
                        'currency' => $this->currency,
                    ]);
                    $this->emit('resetVars');
                }
                else
                    session()->flash('message', 'Invalid Inputs');
            }
            catch(QueryException $ex){
                session()->flash('message', 'Invalid Inputs');
            }
        }
        else
        session()->flash('message', 'Insufficient Balance');
    }

    public function fetchUserAccounts()
    {
        $user=Auth::id();

        $this->user_accounts= Account::where('user_id',$user)
                                ->pluck('id')
                                ->toArray();
    }

    public function fetchCurrencies()
    {
        $client = new Client();
        $res = $client->request('GET', 'https://openexchangerates.org/api/currencies.json');
        foreach(json_decode($res->getBody()) as $key => $val) {
            array_push($this->currencies,$key);
        }
    }

    public function resetVars()
    {
        $this->from='';
        $this->to='';
        $this->amount='';
        $this->currency='';
    }

    public function convertCurrency($amount,$from_currency,$to_currency){
        $params = [
            'q' => "{$from_currency}_{$to_currency}",
            'compact' => "ultra",
            'apiKey' => "a6f234220f61c96a85c8"
        ];

        $client = new Client();
        $res = $client->request('GET', 'https://free.currconv.com/api/v7/convert', ['query' => $params]);

        $rate=1;
        foreach(json_decode($res->getBody()) as $key => $val) {
            $rate=floatval($val);
        }

        return number_format($rate * $amount, 2, '.', '');
      }
}
