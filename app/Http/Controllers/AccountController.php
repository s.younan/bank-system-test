<?php

namespace App\Http\Controllers;
use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function createAccount(Request $request){

        $validator = Validator::make($request->all(), [
            'balance' => ['required','numeric','min:0'],
            'currency' => ['required','string'],
            'user_id' => ['required','exists:users,id'],
        ]);
    
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Inputs!',
                'objects' => [],
                'errors' => $validator->errors(),
                'status_code' => 400
            ], 400);
        }

        $account= Account::create([
            'balance' => request('balance'),
            'currency' => request('currency'),
            'user_id' => request('user_id'),
    
        ]);
        return response()->json([
            'message' => 'Account Created!',
            'objects' => [$account],
            'errors' => [],
            'status_code' => 201
        ], 201);
    }

    public function updateAccount(Request $request){


        $validator = Validator::make($request->all(), [
            'id' => ['required','exists:accounts,id'],
            'balance' => ['required','numeric','min:0'],
            'currency' => ['required','string'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid Inputs!',
                'objects' => [],
                'errors' => $validator->errors(),
                'status_code' => 400
            ], 400);
        }
            
        $account= Account::find(request('id'));
        $account->update([
            'balance' => request('balance'),
            'currency' => request('currency'),
    
        ]);
        return response()->json([
            'message' => 'Account Updated!',
            'objects' => [$account],
            'errors' => [],
            'status_code' => 200
        ], 200);
        
    }

    public function getAccount(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => ['required','exists:accounts,id']
        ]);
    
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Account Not Found!',
                'objects' => [],
                'errors' => $validator->errors(),
                'status_code' => 400
            ], 400);
            
        }

        $account=Account::find(request('id'));
        return response()->json([
            'message' => 'Account Found!',
            'objects' => [$account],
            'errors' => [],
            'status_code' => 200
        ], 200);
    }

    public function getAccounts(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => ['required','exists:users,id']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'User Not Found!',
                'objects' => [],
                'errors' => $validator->errors(),
                'status_code' => 400
            ], 400);
        }

         $accounts = Account::where('user_id', request('id'))->get();

         if (!isset($accounts)) {
            return response()->json([
                'message' => 'Accounts Not Found!',
                'objects' => $accounts,
                'errors' => [],
                'status_code' => 400
            ], 400);
        }
        return response()->json([
            'message' => 'Accounts Found!',
            'objects' => $accounts,
            'errors' => [],
            'status_code' => 200
        ], 200);
    }

    public function deleteAccount(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => ['required','exists:accounts,id']
        ]);
    
        if ($validator->fails()) {

            return response()->json([
                'message' => 'Unsuccessful Delete!',
                'objects' => [],
                'errors' => $validator->errors(),
                'status_code' => 400
            ], 400);
            
        }

        $account=Account::find(request('id'));
        $account->delete();
        return response()->json([
            'message' => 'Successful Delete!',
            'objects' => [$account],
            'errors' => [],
            'status_code' => 200
        ], 200);
   }
}
