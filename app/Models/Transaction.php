<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    function fromAccount(){
        return $this->belongsTo(Account::class, 'from'); 
    }

    function toAccount(){
        return $this->belongsTo(Account::class, 'to'); 
    }

    protected $fillable = [
        'from','to','amount','currency'
    ];
}
