<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function fromTransactions(){
        return $this->hasMany(Transaction::class , 'from');
    }

    public function toTransactions(){
        return $this->hasMany(Transaction::class ,  'to');
    }

    protected $fillable = [
        'balance','currency','user_id'
    ];
}
